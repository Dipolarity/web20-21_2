from flask import Flask, render_template, url_for, request, make_response, redirect
import re



app = Flask(__name__)
application = app


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/args')
def args():
    return render_template('args.html')

@app.route('/headers')
def headers():
    return render_template('headers.html')

@app.route('/form', methods=['GET', 'POST'])
def form():
    return render_template('form.html')

@app.route('/cookies', methods=['GET', 'POST'])
def cookies():
    resp = make_response(render_template('cookies.html'))
    if 'username' in request.cookies:
        resp.set_cookie('username' , 'some name', expires=0)
    else:
        resp.set_cookie('username', 'some name')
    return resp
    
@app.route('/number_form', methods=['GET', 'POST'])
def number_form():
    if request.method=='POST':
        phone=request.form.get('number')
        error_number=''
        if re.fullmatch(r'[+]?\d+[-(.]?\d+[-).]?\d+[.-]?\d+[.-]?\d+',phone):
            pass
        else:
            error_number="Недопустимый ввод. В номере телефона встречаются недопустимые символы."
            return render_template('number_form.html',error_number=error_number)
        if re.fullmatch(r'^.?\d*.?\d{3}.?\d{3}.?\d{2}.?\d{2}$',phone):
               numbers = list(filter(lambda num: num.isdigit(), phone))
               numbers = ''.join(numbers)
        else:
            error_number="Недопустимый ввод. Неверное количество цифр."
            return render_template('number_form.html',error_number=error_number)
        if len(numbers) == 10:
            number='8-{}-{}-{}-{}'.format(numbers[0:3],numbers[3:6],numbers[6:8],numbers[8:])
            return render_template('number_form.html',number_num=number)
        if len(numbers) == 11:
            number='8-{}-{}-{}-{}'.format(numbers[1:4],numbers[4:7],numbers[7:9],numbers[9:11])
            return render_template('number_form.html',number_num=number)
        return render_template('number_form.html',number_num=number)
    return render_template('number_form.html')
