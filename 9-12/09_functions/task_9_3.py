# -*- coding: utf-8 -*-
"""
Задание 9.3

Создать функцию get_int_vlan_map, которая обрабатывает конфигурационный файл коммутатора
и возвращает кортеж из двух словарей:
* словарь портов в режиме access, где ключи номера портов, а значения access VLAN (числа):
{'FastEthernet0/12': 10,
 'FastEthernet0/14': 11,
 'FastEthernet0/16': 17}

* словарь портов в режиме trunk, где ключи номера портов, а значения список разрешенных VLAN (список чисел):
{'FastEthernet0/1': [10, 20],
 'FastEthernet0/2': [11, 30],
 'FastEthernet0/4': [17]}

У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.

Проверить работу функции на примере файла config_sw1.txt


Ограничение: Все задания надо выполнять используя только пройденные темы.
"""


def generate_config(config_filename):
    
    f = open(config_filename,'r')

    access = {}
    trunk = {}
    temp = f.readlines()

    f.close()

    for i in range(len(temp)):
        if temp[i] != '!':
            if temp[i].find('FastEthernet') != -1 and temp[i+1].find("trunk") != -1:
                trunk[temp[i][10:].rstrip()] = temp[i+2][temp[i+2].find("vlan")+5:].rstrip().split(',')
            elif temp[i].find('FastEthernet') != -1 and temp[i+1].find("access") != -1:
                access[temp[i][10:].rstrip()] = str(temp[i+2][temp[i+2].find("vlan")+5:].rstrip())

    return trunk,access

print(generate_config("config_sw1.txt"))
            
