# -*- coding: utf-8 -*-
"""
Задание 6.2b

Сделать копию скрипта задания 6.2a.

Дополнить скрипт:
Если адрес был введен неправильно, запросить адрес снова.

Ограничение: Все задания надо выполнять используя только пройденные темы.
"""

err = True
while err == True:
    err = False
    ip_in = input('Введите IP адрес: ')
    ip_sp = ip_in.split('.')
    print(ip_sp)
    count = 0
    for i in ip_sp:
        count += 1
        
    if count !=4:
        err = True
    else:
        for i in ip_sp:
            if int(i) not in range(1,256,1):
                err = True
        
    if err ==True:
        print('Неправильный IP-адрес')
        ip_sp = 0
        
if int(ip_sp[0]) in range(1,224,1):
    print('unicast')
    
elif int(ip_sp[0]) in range(224,240,1):
    print('multiicast')
    
elif ip_in == '255.255.255.255':
    print('local broadcast')
    
elif ip_in == '0.0.0.0':
    print('unassigned')
    
else:
    print('unused')
