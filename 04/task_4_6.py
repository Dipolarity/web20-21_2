# -*- coding: utf-8 -*-
"""
Задание 4.6

Обработать строку ospf_route и вывести информацию на стандартный поток вывода в виде:
Prefix                10.0.24.0/24
AD/Metric             110/41
Next-Hop              10.0.13.3
Last update           3d18h
Outbound Interface    FastEthernet0/0

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

ospf_route = "      10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0"

route = ospf_route.split()

route.pop(2)

namelist=['Prefix','AD/Metric','Next-Hop','Last update','Outbound Interface']

print("{:22} {:22}".format(namelist[0],route[0]))
print("{:22} {:22}".format(namelist[1],route[1]))
print("{:22} {:22}".format(namelist[2],route[2]))
print("{:22} {:22}".format(namelist[3],route[3]))
print("{:22} {:22}".format(namelist[4],route[4]))
print("/n")
