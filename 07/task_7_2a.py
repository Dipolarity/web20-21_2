# -*- coding: utf-8 -*-
"""
Задание 7.2a

Сделать копию скрипта задания 7.2.

Дополнить скрипт:
  Скрипт не должен выводить команды, в которых содержатся слова,
  которые указаны в списке ignore.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

ignore = ["duplex", "alias", "Current configuration"]

f = open('config_sw1.txt','r')

temp =''
deletelist =''

for line in f:
    if line.startswith('!')==False:
        if not any(i in line for i in ignore):
            print(line.rstrip())
            
#s1 = temp.splitlines()
#s2 = deletelist.splitlines()

#s3 = [line for line in s1 if line not in s2]

#print('\n'.join(s3))

f.close()
